%% Analysis script for Bowtie-Maze data
%% load open ephys data

% clean up the mess
clear all
close all
clc

% make sure to only analyze data if there are no changed files in the
% analysis directory
[~, git_status] = system(['git -C ' get_analysis_folder ' status']);

if (~isempty(strfind(git_status, 'Changes not staged for commit:')) || ...
        ~isempty(strfind(git_status, 'Changes to be committed:')))
    error(['You changed something in the bowtie_analysis repository. '...
        'Please commit all changes before starting an analysis!'])
end
clear git_status

% get the current hash of the bowtie_analysis repository
[~, git_repository_hash] = system(['git -C ' get_analysis_folder ' rev-parse HEAD']);
fprintf('Using bowtie_analysis version %s', git_repository_hash)


run = 1;
% sessions = input('Session number: ');

channel_mapping_file = 'channelMappingATLAS-4shanks+2shanks.csv';

% sampling rate (Hz) to which data should be downsampled to
downsampled_rate = 500;
minimum_exploration_time = 0.1; % min time between explorations in seconds

recalc_neural_signal = false; % set to to to also reload and calculate neural data

if recalc_neural_signal
    
    % design the filter objects to filter LFP data
    filters.low_frequency = getFilter(downsampled_rate, 6, 8, 'low');
    filters.theta = getFilter(downsampled_rate, 6, 12, 'band');
    filters.low_gamma = getFilter(downsampled_rate, 23, 40, 'band');
    filters.high_gamma = getFilter(downsampled_rate, 55, 95, 'band');
end

processor_id = 100;
experiment_folder = sprintf('N:\\Bowtie\\Run %d\\', run);
init_dir = pwd; %folder from which the script was run



for session=1:12
    
    disp('***************************************************************')
    fprintf('Working on dataset in %s\n\tSession %02d\n', experiment_folder, session);
    disp('***************************************************************')
    
    
    working_dir = sprintf('%sSession %02d', experiment_folder, session); % data directory for current session
    analysis_folder = get_analysis_folder;
    
    cd(working_dir)
    try
        ephys_folder = dir('B*_session');
        ephys_folder = ephys_folder.name;
    catch
        ephys_folder = uigetdir(working_dir, 'Select folder with session ephys:');
    end
    if recalc_neural_signal
        cd(ephys_folder);
        
        % automatically determine number of channels
        max_channels = 128;
        fprintf('Finding all existing channels between 1 and %d\n', max_channels);
        
        total_loading_time = tic;
        
        chans = zeros(max_channels, 1);
        for c=1:max_channels
            chans(c) = ~isempty(dir([num2str(processor_id) '_CH', num2str(c), '.continuous']));
        end
        
        chans = find(chans);
        
        % check if KWIK format is used instead of OE format
        if isempty(chans)
            error('No open ephys files found. Implement loading of kwik file here...');
        end
        disp('Loading channels and downsampling')
        tic;
        [tmp_data, timestamps, info] = load_open_ephys_data([num2str(processor_id) '_CH1.continuous']);
        downsampling_factor = round(info.header.sampleRate/downsampled_rate);
        
        sampling_rate = info.header.sampleRate/downsampling_factor;
        
        % check how much the actual downsampling rate differs from desired one
        downsampling_error = abs(sampling_rate-downsampled_rate)/downsampled_rate;
        fprintf('Original sampling rate: %d\nDownsampling error: %03.3f (desired=%3d Hz, real=%3d Hz)\n', ...
            info.header.sampleRate, downsampling_error, downsampled_rate, sampling_rate);
        
        %downsample timestamps
        timestamps = timestamps(1:downsampling_factor:end);
        
        % preallocate data
        data = nan(length(timestamps), length(chans));
        data_low_freq = nan(size(data));
        data_theta = nan(size(data));
        data_low_gamma = nan(size(data));
        data_high_gamma = nan(size(data));
        
        data(:,1) = decimate(tmp_data, downsampling_factor);
        fprintf('\t-> filtering LFP\n')
        c=1;
        data_low_freq(:, c) = filtfilt(filters.low_frequency, data(:, c));
        data_theta(:, c) = filtfilt(filters.theta, data(:, c));
        data_low_gamma(:, c) = filtfilt(filters.low_gamma, data(:, c));
        data_high_gamma(:, c) = filtfilt(filters.high_gamma, data(:, c));
        
        for c=2:length(chans)
            tmp_data = load_open_ephys_data([num2str(processor_id) '_CH', num2str(chans(c)), '.continuous']);
            data(:, c) = decimate(tmp_data, downsampling_factor);
            fprintf('\t-> filtering LFP\n')
            data_low_freq(:, c) = filtfilt(filters.low_frequency, data(:, c));
            data_theta(:, c) = filtfilt(filters.theta, data(:, c));
            data_low_gamma(:, c) = filtfilt(filters.low_gamma, data(:, c));
            data_high_gamma(:, c) = filtfilt(filters.high_gamma, data(:, c));
            
        end
        toc
        
        % calculate the instantaneous phases of the signal
        % phases = angle(hilbert(data));
        tic;
        disp('Calculate instantaneous phases & power of signals...')
        
        hilbert_data_low_freq = hilbert(data_low_freq);
        hilbert_data_theta = hilbert(data_theta);
        hilbert_data_low_gamma = hilbert(data_low_gamma);
        hilbert_data_high_gamma = hilbert(data_high_gamma);
        
        phases_low_freq = angle(hilbert_data_low_freq);
        phases_theta = angle(hilbert_data_theta);
        phases_low_gamma = angle(hilbert_data_low_gamma);
        phases_high_gamma = angle(hilbert_data_high_gamma);
        
        power_low_freq = abs(hilbert_data_low_freq).^2;
        power_theta = abs(hilbert_data_theta).^2;
        power_low_gamma = abs(hilbert_data_low_gamma).^2;
        power_high_gamma = abs(hilbert_data_high_gamma).^2;
        
        clear hilbert_data_low_freq hilbert_data_theta hilbert_data_low_gamma hilbert_data_high_gamma
        toc
        
        fprintf('******************************\n\tTotal loading time %.2f\n******************************', toc(total_loading_time));
        clear tmp_s tmp_f tmp_t
        % toc
        
        [events_data, events_timestamps, events_info] = load_open_ephys_data('all_channels.events');
        trigger_timestamps = calculate_trigger_timestamps(events_timestamps);
        clear tmp_data max_channels c
        
        cd('..');
    else
        total_loading_time = tic;
        disp('Loading neural data from previous analysis... (don''t worry, I keep track of where it''s from...)')
        % if not loaded from raw data, load from existing file
        load('Session_preprocessed.mat', 'power_*', 'phases_*', 'data', 'data_*', 'timestamps', 'events_*', 'trigger_timestamps', 'downsampling_error', 'downsampling_factor', 'sampling_rate','info','ephys_folder', 'filters')
        % track the history of analyses just in case...
        git_repository_hash_history = load('Session_preprocessed.mat', 'git_repository_hash_history');
        if isempty(fieldnames(git_repository_hash_history))
            git_repository_hash_history = load('Session_preprocessed.mat', 'git_repository_hash');
        else
            git_repository_hash_history(end+1) = load('Session_preprocessed.mat', 'git_repository_hash');
        end
        fprintf('******************************\n\tTotal loading time %.2f\n******************************', toc(total_loading_time));
    end
    % load the behavioral data
    disp('Load scoring')
    scoring = load_scoring;
    disp('Load arduino timestamps')
    arduino_timestamps = load_ardunio_timestamps;
    
    %% synchronize data
    [arduino_timestamps, scoring] = synchronize_timestamps(arduino_timestamps, scoring, trigger_timestamps);
    
    fprintf('Get exploration periods (minimum dt=%d ms)\n', minimum_exploration_time);
    exploration_periods = get_exploration_periods(scoring, minimum_exploration_time);
    exploration_length = exploration_periods(2,:)-exploration_periods(1,:);
    exploration_timestamps_pre = zeros(100, size(exploration_periods, 2));
    exploration_timestamps_post = zeros(100, size(exploration_periods, 2));
    for expl=1:size(exploration_periods, 2)
    end
    
    %% load the channel mapping
    mapping = load_mappings([analysis_folder channel_mapping_file]);
    
    %% exploration
    % create a binary vector saying whether a downsampled frame contains exploration
    is_exploration = zeros(size(timestamps));
    for exp_p=1:length(exploration_periods)
        is_exploration = is_exploration | (timestamps >= exploration_periods(1,exp_p) & timestamps <= exploration_periods(2,exp_p));
    end
    
    
    timestamps_t0.pre = zeros(100, size(exploration_periods,2));
    timestamps_t0.post = zeros(100, size(exploration_periods,2));
    timestamps_t0.info = '100 samples (200ms) pre and post beginning of each exploration period (trial)';
    for expl=1:size(exploration_periods,2)
        t0=get_closest_timestamp(exploration_periods(1,expl), scoring.video_timestamps_in_oe_timeframe);
        timestamps_t0.pre(:,expl) = t0-100:t0-1;
        timestamps_t0.post(:,expl) = t0+1:t0+100;
    end
    
    %% stimulus and trial
    stimulus_at_sample = nan(size(timestamps));
    trial_at_sample = nan(size(timestamps));
    for t=1:length(timestamps)
        [tmp_stim, tmp_tr, ~] = get_stimulus_at_t(timestamps(t), scoring);
        if isempty(tmp_tr), tmp_tr=nan; end
        if isempty(tmp_stim), tmp_stim=nan; end
        trial_at_sample(t) = tmp_tr;
        stimulus_at_sample(t) = tmp_stim;
    end
    stimulus_at_sample(find(isnan(stimulus_at_sample))) = 0;
    trial_at_sample(find(isnan(trial_at_sample))) = 0;
    
    disp('Noise detection')
    % noise is where 30% of the abs(data) channels are > 4.5 std
    noise_at_timestamp = detect_noise_semiauto(data(:,1:64), 4.5, 0.3, 100, true);
    
    
    %     %% detect artifacts
    %
    %     figure;
    %     plot(data(:,1))
    %     set(gcf, 'Position', [1, 41, 1920, 963])
    %     axis tight
    %     is_artifact = mark_artifacts(timestamps);
    %
    %     %% plot artifacts and exploration
    %     hold on
    %     plot(find(is_artifact), zeros(size(find(is_artifact))), '*')
    %     plot(find(is_exploration), zeros(size(find(is_exploration))), '*')
    %     legend({'', 'Artifacts', 'Exploration'})
    %     saveas(gcf, [analysis_folder 'Analyses\Artifacts-Exploration-R1-S' num2str(session) '.png'])
    %
    
    %% save preprocessed data
    save('Session_preprocessed.mat', '-v7.3')
end

% calculate behavioral variables
script_pooled_behavior


