function trigger_timestamps = calculate_trigger_timestamps(timestamps_or_eventsfile)

%  Door open: 35ms high, 35ms low, 70ms high
%  Door close: 70ms high, 35ms low, 35ms high
%  Sound: 35ms high, 35ms low, 35ms high, 35ms low, 35ms high
%  Expt start/end: 50ms high, 25ms low, 25ms high, 50ms low, 50ms high
if nargin == 0
    eventfile = dir('all_channels.events');
        if numel(eventfile) == 1
            filename = eventfile.name;
        else
            error('You have to supply either a path to the events file, a ist of trigger timestamps, or run the function in a folder with the all_channels.events file present.')
        end
         [~, timestamps, ~]  = load_open_ephys_data(filename);
else
    validateattributes(timestamps_or_eventsfile, {'char', 'numeric'}, {});
    if ischar(timestamps_or_eventsfile)
        [~, timestamps, ~]  = load_open_ephys_data(timestamps_or_eventsfile);
    else
        timestamps = timestamps_or_eventsfile;
    end
end



td = round(diff(round(timestamps*100000))/100);

patterns = struct(...
    'start_end', [50, 25, 25, 50, 50], ...
    'door_open', [35, 35, 70], ...
    'door_close', [70, 35, 35], ...
    'sound', [35, 35, 35, 35, 35]);

fns = fieldnames(patterns);
trigger_timestamps = struct();

for p=1:length(fns)
    pattern_name = fns{p};
    pattern = patterns.(pattern_name);
    trigger_timestamps.(pattern_name) = [];
    for i=1:length(td)-length(pattern)+1
        if all(td(i:i+length(pattern)-1)==pattern') 
        trigger_timestamps.(pattern_name) = [trigger_timestamps.(pattern_name) timestamps(i)];
        end
    end
end