function time_ms = convert_bonsai_timestamp(bonsai_timestamp)
%%Convert timestamps in Bonsai format to milliseconds
% Currently ignores the time information, i.e., timestamps at a specific
% time are the same for each day
% input time format: YYYY-MM-DDThh:mm:ss.sssssss+00:00
	bonsai_timestamp = strtrim(bonsai_timestamp);
    tmp_date = ...
        regexp(bonsai_timestamp, ...
        '(?<Y>\d{4})-(?<M>\d{2})-(?<D>\d{2})T(?<h>\d{2}):(?<m>\d{2}):(?<s>\d{2}.\d+).*', 'names');
    time_ms = ((str2double(tmp_date.h) * 60 + str2double(tmp_date.m))...
                   * 60 + str2double(tmp_date.s)) * 1000;