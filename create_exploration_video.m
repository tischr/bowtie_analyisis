function create_exploration_video(video_reader, scoring)
    downsampled_fileName = [video_reader.Name(1:end-4) '-exploration.avi'];
    if exist(downsampled_fileName)
        error([downsampled_fileName ' already exists.'])
    end
    video_writer = VideoWriter(downsampled_fileName);
    
    current_frame = 1;
    set(video_reader, 'CurrentTime', 0);
    frame = readFrame(video_reader);
    frame_small = im2frame(imresize(frame, 1/4));
    resized_video = repmat(frame_small, length(scoring.framesExplore), 1);
    n_exploration_frames = 0;
    while hasFrame(video_reader)
        current_frame = current_frame + 1;
        frame = readFrame(video_reader);
        if ~isempty(find(scoring.framesExplore==current_frame, 1))
            n_exploration_frames = n_exploration_frames + 1;
            frame_small = im2frame(imresize(frame, 1/4));
            resized_video(n_exploration_frames) = frame_small;
        end
    end
    
    writeVideo(video_writer, resized_video);
    