function noise_window = detect_noise_semiauto(data, noise_std_threshold, noise_ratio_threshold, window_size, fully_automatic)
    % Semi-automatic noise detection
    % First, detect all samples which cross the threshold, take a window around
    % them
    % Second, interactively have an experimenter validate and refine noise
    % windows
    
    if nargin < 5
        fully_automatic = false;
    end
    
    if nargin < 4 || isempty(window_size) %|| ~validateattributes(window_size, {'numeric'}, {'nonnegative'})
        window_size = 100;
    end
    
    if nargin < 3 || isempty(noise_ratio_threshold) %|| ~validateattributes(noise_ratio_threshold, {'numeric'}, {'>',0,'<=',1})
        noise_ratio_threshold = 0.55;
    end
    
    if nargin < 2 || isempty(noise_std_threshold) %|| ~validateattributes(noise_std_threshold, {'numeric'}, {'nonnegative'})
        noise_std_threshold = 2;
    end
    
    display_window_size = 10000; % number of samples to plot for manual step
    minimum_display_offset = 1000; % if there are artifacts detected closer to beginning of plot than this, also plot earlier range...
    
    % get the ratio of channels that surpass the threshold
    is_noise_auto = sum(abs(data) > noise_std_threshold*repmat(std(data), size(data,1), 1),2)./size(data,2);
    
    % take a window
    noise_window = conv(double(is_noise_auto > noise_ratio_threshold), rectwin(2*window_size), 'same');
    noise_window = [noise_window(window_size:end); noise_window(end)*ones(window_size-1,1)];
    
    
    % manual correction
    if ~fully_automatic
        corrected_limits = [];
        noise_detection_figure = figure('Name', 'Manual noise correction');
        set(gcf, 'Position', [80, 50, 1750, 940])
        
        display_limits = [-display_window_size+1 0];
        while display_limits(1) < size(data,1)
            display_limits = display_limits + display_window_size;
            
            if display_limits(1) >= size(data, 1) % done...
                break;
            end
            
            if display_limits(2) > size(data, 1) % almost done
                display_limits(2) = size(data, 1);
            end
            
            if all(noise_window(display_limits(1):display_limits(2))==0) % skip if nothing is in here
                %                 disp('no noise')
                continue;
            else
                % shift window if there's noise in the beginning to make
                % sure everything is displayed properly
                if any(noise_window(display_limits(1):display_limits(1)+minimum_display_offset) > 0)
                    display_limits = display_limits-minimum_display_offset;
                end
                hold off
                plot(data(:,1))
                hold on
                plot(find(noise_window > 0 & noise_window <= 1), data(find(noise_window > 0 & noise_window <= 1), 1), 'g.')
                plot(find(noise_window > 1 & noise_window <= 3), data(find(noise_window > 1 & noise_window <= 3), 1), 'y.')
                plot(find(noise_window > 3), data(find(noise_window > 3), 1), 'r.')
                xlim(display_limits);
                drawnow;
                [is_artifact_fragment, ~, ~] = ginput;
                try
                    [is_artifact_fragment, ~, ~] = ginput;
                    is_artifact_fragment = reshape(is_artifact_fragment, 2, []);
                catch
                    while rem(numel(is_artifact_fragment),2)
                        [is_artifact_fragment, ~, ~] = ginput;
                        is_artifact_fragment = reshape(is_artifact_fragment, 2, []);
                    end
                end
                for n_fragments=1:size(is_artifact_fragment, 2)
                    is_artifact_fragment(1, n_fragments) = max(display_limits(1), is_artifact_fragment(1, n_fragments));
                    is_artifact_fragment(2, n_fragments) = min(is_artifact_fragment(2, n_fragments), display_limits(2));
                    corrected_limits = [corrected_limits is_artifact_fragment(1, n_fragments):is_artifact_fragment(2, n_fragments)];
                end
                plot(corrected_limits, zeros(size(corrected_limits)), '*')
                pause(0.5)
            end
        end
        close(noise_detection_figure);
        
        noise_window = zeros(size(data,1),1);
        noise_window(int32(corrected_limits)) = 1;
    end