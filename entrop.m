function e = entrop(X)
%ENTROP Summary of this function goes here
%   Detailed explanation goes here
% written: some time in 2009 for BA thesis work :)
% adapted 2016-06-18

% e = 0;
% 
% 
% %pX = X/sum(X);
% 
% for x=1:length(X)
%     p = length(X(X==X(x)))/length(X);
%     if p ~= 0
%         e = e - p * log2(p);
%     end
% end

p = histcounts(X, numel(unique(X)), 'BinMethod','integers', 'Normalization', 'probability');
e = -sum(p.*log2(p));
end

