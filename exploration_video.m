% play a video of all frames in OE time that contain exploration
figure('Name', 'Exploration Frames')

% current_frame = 0;
% set(video_reader, 'CurrentTime', 0);
% fprintf('        ');
% exploration_times_from_video = [];
% last_frame_in = false;
while hasFrame(video_reader)
%     fprintf('\b\b\b\b\b\b\b\b%8d', current_frame)
    frame = readFrame(video_reader);
    current_frame = current_frame + 1;
    if ~isempty(find(scoring.framesExplore==current_frame, 1))
        if ~last_frame_in
            fprintf('Exploration starts at %.2f ', video_reader.CurrentTime);
            last_frame_in = true;
            exploration_times_from_video(end+1, [1 2]) = [video_reader.CurrentTime nan];
        end
        image(frame)
        drawnow;
    else
        if last_frame_in
            last_frame_in = false;
            fprintf('and ends at %.2f\n', video_reader.CurrentTime);
            exploration_times_from_video(end, 2) = video_reader.CurrentTime;
        end
    end
end
clear current_frame frame
% 
% expl_per = 1;
% frame = readFrame(video_reader);
% img = image(frame);
% 
% % go through all exploration periods and show the videos
% while expl_per < length(exploration_periods)
%     set(video_reader, 'CurrentTime',  exploration_periods(1,expl_per)-timestamps(1)-scoring.video_timestamps_in_oe_timeframe(1))
%     while video_reader.CurrentTime < exploration_periods(2, expl_per)
%         frame = readFrame(video_reader);
%         set(img, 'CData', frame)
%         drawnow;
%     end
%     expl_per = expl_per+1;    
% end
% 
% clear expl_per frame img

% frame = video_at_oe_time(video_reader, scoring.video_timestamps_in_oe_timeframe(scoring.framesExplore(1)), scoring, timestamps);
% img = image(frame);
% for fr=2:length(scoring.framesExplore)
%     frame = video_at_oe_time(video_reader, scoring.video_timestamps_in_oe_timeframe(scoring.framesExplore(fr)), scoring, timestamps);
%     set(img, 'CData', frame);
%     drawnow;
% end