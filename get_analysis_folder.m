function [f, n] = get_analysis_folder
    %% get_analysis_folder
    % Returns the path and the name of the folder in which the analysis scripts
    % (including this one) are located.
    % Output:
    % f: path of the folder
    % n: name of the folder
    
    f = mfilename('fullpath');
    xloc = strfind(f, '\');
    n = f(xloc(end-1)+1:xloc(end)-1);
    f = f(1:xloc(end));
    
    