function ind = get_closest_timestamp(t, timestamps)
    ind = find(timestamps-t<0, 1, 'last');
    