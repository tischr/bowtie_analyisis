function exploration_periods = get_exploration_periods(scoring, minimum_time_between_periods)
% Given a synchronized scoring structure and a minimum inter-exploration
% interval, return the start and end timestamps of exploration periods
% Inputs:
% * scoring: a scoring structure that was synchronized to open ephys time
% * minimum_time_between_periods: minimum time (in s) between two times so
%   that they are assigned to different exploration bouts
% Output:
% Nx2 matrix containing start and end of N exploration bouts


    if nargin <2
        % default: 100 ms of not exploring = end of exploration period
        minimum_time_between_periods = 0.1;
    end
    
    scoring.framesExplore = sort(scoring.framesExplore);
    exploration_timestamps = scoring.video_timestamps_in_oe_timeframe(scoring.framesExplore);
    
    exploring_phase_start = find([1 diff(exploration_timestamps) > minimum_time_between_periods]);
    exploring_phase_end = find([diff(exploration_timestamps) > minimum_time_between_periods 1]);
    
    exploration_periods = [exploration_timestamps(exploring_phase_start); exploration_timestamps(exploring_phase_end)];