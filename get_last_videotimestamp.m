function index = get_last_videotimestamp(t, scoring)
    if nargin==1
        if evalin('base', 'exist(''scoring'',  ''var'')')
            scoring = evalin('base', 'scoring');
        else
            error('scoring struct with timestamps not found');
        end
    end
    
    index = find(scoring.video_timestamps_in_oe_timeframe<t, 1, 'last');
