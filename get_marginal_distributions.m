function varargout = get_marginal_distributions(P)
%get_marginal_distributions calculates the marginal distributions of a 2D
%or 3D probability distribution
%  for 3D use [pXX1 pXY pX1Y pX pX1 pY] = get_marginal_distributions(P)
%  for 2D use [pX pY] =  get_marginal_distributions(P)
% written: some time in 2009 for BA thesis work :)

if ndims(P) == 3
    pXX1Y = P;
    % calculate the marginal probabilities
    pXY  = sum(pXX1Y, 2);
    tmp = [];
    for i = 1:size(pXY, 3)
    	tmp = [tmp pXY(:,:,i)];
    end
    
    pXY = tmp; %[pXY(:,:,1) pXY(:,:,2) pXY(:,:,3)];
    pXX1 = sum(pXX1Y, 3);
    pX1Y = sum(pXX1Y, 1);
    pX1Y = [pX1Y(:,:,1); pX1Y(:,:,2); pX1Y(:,:,3)];
    
    pX = sum(pXY, 2);
    pY = sum(pXY, 1);
    pX1 = sum(pXX1, 1);    

    varargout =  {pXX1 pXY pX1Y pX pX1 pY};
elseif ndims(P) == 2
    % calculate the marginal probabilities
    pX  = sum(P,2);
    pY = sum(P);
    varargout = {pX pY};
else
    error(['Dimension of distribution is too high.'...
    'Please use only 2D or 3D matrices!']);
end

end

