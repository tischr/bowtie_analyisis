function [stimulus, trial_number, img] = get_stimulus_at_t(t, scoring)
    
    % preload all stimulus images
    persistent stimulus_images;
    if isempty(stimulus_images)
        % load the stimulus image
        stimulus_images_path = mfilename('fullpath');
        xloc = strfind(stimulus_images_path, '\');
        xloc = xloc(end);
        stimulus_images_path = [stimulus_images_path(1:xloc) 'stimuli\\'];
        for i=0:24
            stimulus_images{i+1} = imread(sprintf('%s%d.jpg', stimulus_images_path, i));
        end
    end
    
    validateattributes(t, {'numeric'}, {});
    validateattributes(scoring, {'struct'}, {});
    
    if ~isfield(scoring, 'video_timestamps_in_oe_timeframe')
        error('Synchronize your data to the open ephys time frame first!')
    end
    
    trial_number =  find(scoring.trial_timestamps<t, 1, 'last');
    stimulus = scoring.trialObjects(trial_number);
    if isempty(stimulus)
        img = stimulus_images{1};
    else
        img = stimulus_images{stimulus+1};
    end
    
    
    
