function pXY = joint_probability_matrix2D(X, Y, offset)
%joint_probability_matrix2D creates a joint probability matrix of two
%signals. The offset specifies the number of elements the vector Y is
%shifted.
% written: some time in 2009 for BA thesis work :)

% negative offset is basically shifting the other way round
if offset < 0
    tmp = X;
    X = Y;
    Y = tmp;
    offset = -offset;
end

% shift the two time series
X = X(1 + offset:length(X));
Y = Y(1:length(Y) - offset);

% calculate the joint probability distribution
% pXY = sparse(max(X), max(Y), length(X));

indices = [X Y];
indices = indices(sum(~isnan(indices),2) == 2,:); % throw out nan pairs
sumElem = size(indices,1);

pXY = sparse(indices(:,1), indices(:,2), ones(sumElem,1));


% now go through X and Y and count co-occurrences of data
% for i=1:length(X)
%     some points are NaN bins corresponding to missing data in the binning
%     process
%     if ~isnan(X(i)) && ~isnan(Y(i))
%         pXY(X(i), Y(i)) = pXY(X(i), Y(i)) + 1;
%     end
% end

% sumElem = sum(sum(pXY));

% create a probability distribution from the frequency distribution
pXY = pXY / sumElem;
end
