function pXX1Y = joint_probability_matrix3D(X, Y, lag)
%joint_probability_matrix3D creates a joint probability matrix of three
%signals X, X+lag, and Y.
% written: some time in 2009 for BA thesis work :)

%some requirements:
if length(X) ~= length(Y)
    fprintf(['ERROR: X and Y must have the same length. '...
        'length(X)=%d, length(Y)=%d'], length(X), length(Y));
    pXX1Y = -1;
    return;
end

if lag > length(X)-1
    fprintf(['ERROR: lag too high. This won''t yield reasonable results.'...
        '(length(X)=%d, lag=%d)]'], length(X), lag); 
    %and, in addition, may result in an error
    pXX1Y = -1;
    return;
end

% the 95% is just arbitrary by now - is there a justified maximal lag, or
% should it just be left out?
if lag > round(length(X) * 0.95)
    disp(['Warning: the lag selected is quite high compared to the'... 
        'length of the signals. You might want to chose a smaller lag.']);
end


% calculate the joint probability distribution

pXX1Y = zeros(max(X), max(X), max(Y)); 

if (lag == 0)
    for i=1:length(X)-1
        pXX1Y(X(i), X(i+1), Y(i)) = pXX1Y(X(i), X(i+1), Y(i)) + 1;
    end
elseif (lag < 0)
    for i=1-lag:length(X)-1
        pXX1Y(X(i), X(i+1), Y(i+lag)) = pXX1Y(X(i), X(i+1), Y(i+lag)) + 1;
    end
elseif (lag > 0)
    for i=1:length(X)-lag
        pXX1Y(X(i), X(i+1), Y(i+lag)) = pXX1Y(X(i), X(i+1), Y(i+lag)) + 1;
    end
else
    disp('Something went wrong. Probably the lag is no integer number.');
    pXX1Y = NaN;
    return;
end

% make it a probability distribution
sumElem = sum(sum(sum(pXX1Y)));
pXX1Y = pXX1Y / sumElem;

end
