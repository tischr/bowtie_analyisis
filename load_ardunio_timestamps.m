function [timestamps, lines] = load_ardunio_timestamps(filename)
%%Load data from an event file created by the Bowtie maze Arduino control
%%script.

    if nargin == 0 
        csvfiles = dir('*ArduinoEvents*.csv');
        if numel(csvfiles) == 1
            filename = csvfiles.name;
        else
            error('You have to specify a filename or run the command in a folder with exactly one matching file.')
        end
    end

    f = fopen(filename, 'r');
    lines = {};
    timestamps = {};
    
    while ~feof(f)
        line = fgetl(f);
        extracted_data = regexp(line, '(?<arduino_time>\d+): (?<event>.+) (?<bonsai_time>\d{4}-\d{2}-\d{2}T\d{2}.*)', 'names');
        
        % only continue if all fields were found
        if ~all(size(extracted_data))
            continue;
        end
        
        extracted_data.bonsai_time = convert_bonsai_timestamp(extracted_data.bonsai_time);
        extracted_data.arduino_time = str2double(extracted_data.arduino_time);
        
        if strfind(line, 'Enter A to start experiment')
            event_type = 'other';
        elseif  strfind(line, 'Session started')
            event_type = 'session_start';
        elseif  strfind(line, 'Opening doors')
            event_type = 'door_opening';
        elseif  strfind(line, 'Doors open')
            event_type = 'door_open';
        elseif  strfind(line, 'Animal reached other compartment')
            event_type = 'animal_crossed';
        elseif  strfind(line, 'Doors closed')
            event_type = 'door_closed';
        elseif  strfind(line, 'Trial')
            event_type = 'trial_start';
            tmp = regexp(extracted_data.event, 'Trial: (?<trial_number>\d+) - (?<trial_length>\d+) s', 'names');
            extracted_data.trial_number = str2double(tmp.trial_number);
            extracted_data.trial_length = str2double(tmp.trial_length);
        elseif  strfind(line, 'Experiment ended')
            event_type = 'session_end';
        else
            event_type = 'other';
        end
        
        if ~isfield(timestamps, event_type)
            timestamps.(event_type) = extracted_data;
        else
            timestamps.(event_type) = [timestamps.(event_type), extracted_data];
        end
        lines = [lines, line];
    end

end