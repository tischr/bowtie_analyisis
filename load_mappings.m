function mappings = load_mappings(filename)
    % Load the channel mappings from the files in the current folder.
    % Input:
    % String filename: name of the mapping file to be loaded
    %                  if not supplied (or if the supplied file does not
    %                  exist) 

    mappings = [];
    if nargin > 0 && exist(filename, 'file')
        mappings = readtable(filename);
        % for mapping files starting with channelMapping and ending with
        % .csv, this gives the middle part. If the file is named
        % differently, you might get a nonsense description here
        mappings.Properties.Description = ...
            filename(max([1,15]):min([length(filename)-4, length(filename)]));
    else
        if nargin > 0 && ~exist(filename, 'file')
            warning('The filename you supplied does not exist. Function will return all files found in the current folder!');
        end
        
        filenames = dir('channelMapping*.csv');
        for i=1:length(filenames)
            filename = filenames(i).name;
            mappings{i} = readtable(filename);
            mappings{i}.Properties.Description = ...
                filename(max([1,15]):min([length(filename)-4, length(filename)]));
        end
    end