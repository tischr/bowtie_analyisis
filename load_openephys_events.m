function events = load_openephys_events(filename)
    % should be douplicate of calculate_trigger_timestamp - accidentally
    % wrote this one as well...
    if nargin == 0
        eventfiles = dir('all_channels.events');
        if numel(eventfiles) == 1
            filename = eventfiles.name;
        else
            error('You have to specify a filename or run the command in a folder with exactly one matching file.')
        end
    end
    
%     Door open: 35ms high, 35ms low, 70ms high
%     Door close: 70ms high, 35ms low, 35ms high
%     Sound: 35ms high, 35ms low, 35ms high, 35ms low, 35ms high
%     Expt start/end:  50ms high, 25ms low, 25ms high, 50ms low, 50ms high

    patterns = struct(...
        'start_end', [50, 25, 25, 50, 50],...
        'door_open', [35, 35, 70],...
        'door_close', [70, 35, 35],...
        'sound', [35, 35, 35, 35, 35]);
    
    fn = fieldnames(patterns);
    
    [data, timestamps, info] = load_open_ephys_data(filename);
    dt = diff(round(timestamps*1000));
    
    events = struct();
    for p=1:length(patterns)
        events.(fn(p)) = [];
        pat = patterns.(fn(p));
        for s=1:length(dt)-length(pat)+1
            if all(dt(s:s+length(pat)) == pat)
                events.(fn(p)) = [events.(fn(p)) s];
            end
        end
    end