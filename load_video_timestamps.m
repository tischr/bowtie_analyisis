function video_timestamps = load_video_timestamps(filename)
f = fopen(filename);
video_timestamps = [];
while ~feof(f)
    video_timestamps = [video_timestamps convert_bonsai_timestamp(fgetl(f))];
end
