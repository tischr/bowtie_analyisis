function is_artifact = mark_artifacts(timestamps)
    [x, ~, ~] = ginput;
    x(1) = max(x(1), 0);
    x(end) = min(x(end), length(timestamps));
    is_artifact = zeros(size(timestamps));
    x = reshape(x, 2, []);
    for i=1:length(x)
        is_artifact(x(1,i):x(2,i)) = 1;
    end
