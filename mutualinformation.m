function [miOrig, miR, miPT, offset] = mutualinformation(X, Y, offset, assume_correct_input)
    %MUTUALINFORMATION Calculates the mutual information of two time series X
    %and Y
    %   Given two time series, the time delayed mutual information between them
    %   is calculated. offset specifies the number of data points in which Y is
    %   shifted.
    %   The function returns 3 vectors.
    %   miOrig contains the uncorrected mutual information
    %   miR contains the Roulston corrected mutual information
    %   miPT contains the Panzeri-Treves corrected mutual information
    % written: some time in 2009 for BA thesis work :)
    
    miOrig = nan(length(offset),1);
    miR = nan(length(offset),1);
    miPT = nan(length(offset),1);
    
    if nargin < 4 || ~assume_correct_input
        for i=1:length(offset)
            %     if length(unique(X)) == 1 || length(unique(Y)) == 1
            if any(diff(X(:)>0)) || any(diff(Y(:)>0))
                miOrig(i) = nan;
                miR(i) = nan;
                miPT(i) = nan;
            else
                try
                    [miOrig(i), miR(i), miPT(i)] = mi(X, Y, offset(i));
                catch
                    miOrig(i) = nan;
                    miR(i) = nan;
                    miPT(i) = nan;
                end
            end
        end
    else
        for i=1:length(offset)
            try
                [miOrig(i), miR(i), miPT(i)] = mi(X, Y, offset(i));
            catch
                miOrig(i) = nan;
                miR(i) = nan;
                miPT(i) = nan;
            end
        end
    end
end

function [miOrig, miR, miPT] = mi(X, Y, offset)
    % now, go on
    try
        pXY = joint_probability_matrix2D(X, Y, offset);
        [pX, pY] =  get_marginal_distributions(pXY);
    catch % if the joint PDF cannto be created
        miOrig =NaN;
        miR = NaN;
        miPT = NaN;
        return
    end
    
    % Correction term (Roulston, 1999; Jin et al, 2009)
    if offset < 0
        X2 = Y;
        Y2 = X;
        offset2 = -offset;
    else
        X2 = X;
        Y2 = Y;
        offset2 = offset;
    end
    
    nX = max(X2(1 + offset2:length(X)));
    nY = max(Y2(1:length(Y) - offset2));
    nXY = numel(find(pXY));
    correctionTermRoulston  = (nX + nY - nXY - 1)/(2*numel(X));
    correctionTermPT  = nXY/(2*numel(X)*log(2));
    
    % sometimes (especially in the sliding window condition) only one bin is left for some time series.
    % Then, no calculation can be carried out
    if numel(pX) == 1 && numel(pY) == 1
        miOrig = nan;
        miR = nan;
        miPT = nan;
        return
    end
    
    % completely vectorized form of MI
    [row, col, v] = find(pXY);
    x = pX(row);
    y = pY(col)';
    lV = spfun(@log2, v);
    try
        lXY = spfun(@log2, x .* y);
    catch
        lXY = spfun(@log2, x' .* y);
    end
    
    miOrig = full(sum(v .* (lV - lXY)));
    miR = miOrig + correctionTermRoulston;
    miPT = miOrig - correctionTermPT;
    
end

