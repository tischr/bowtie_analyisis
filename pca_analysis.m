function [data__pca_coeffs, data_pca_score, data_pca_explained] = pca_analysis(data, scatter_color)
    % Calculate the PCA, plot the first 3 components in 3D space
    [data__pca_coeffs, data_pca_score, ~, ~, data_pca_explained] = pca(data);
    
    if nargout == 0
%         figure;
        scatter3(data_pca_score(:,1), data_pca_score(:,2), data_pca_score(:,3), 1, scatter_color);
        xlabel('1th Component')
        ylabel('2th Component')
        zlabel('3th Component')
%         colormap([0.8 0.8 0.8; 0.2 0.1 0.1]);
        drawnow;
    end