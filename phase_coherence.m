function C = phase_coherence(X)
% Calculate the phase coherence between the columns of matrix X. X contains
% the phases of a signal obtained by phase(hilbert(x))
    C = abs(mean(exp(1i*X),2));