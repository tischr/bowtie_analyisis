figure(1)
plot(timestamps, data(:,1));
axis tight
xlabel('Time (seconds)')
ylabel('LFP (uV)')
hold on

y = get(gca, 'YLim');
for expl=1:size(exploration_periods, 2)
    % exploration start
    x = exploration_periods(1, expl);
    vertical_line(x, y(1), y(2), 'r');
    
    % exploration end
    x = exploration_periods(2, expl);
    vertical_line(x, y(1), y(2), 'b');
end
% remove temp vars
clear x y expl