function plot_trial_starts(arduino_timestamps, scoring)
    all_times = [arduino_timestamps.session_start.bonsai_time/1000 [arduino_timestamps.trial_start.bonsai_time]/1000];
    all_events = {arduino_timestamps.trial_start.event};
    for i=1:length(all_events)
        tmp = regexp(all_events(i), 'Trial: (?<trial>\d+) - (?<length>\d+) s', 'names');
        trial_length(i) = str2double(tmp{1}.length);
    end
    dt = diff(all_times);
    del = dt-trial_length;

    figure()
    subplot(2+nargin,1,1)
    plot(all_times, '*')
    title('Trial start times')
    ylabel('Time (s)')
    xlabel('Trial number')
  
    subplot(2+nargin,1,2)
    plot([dt; trial_length]', ':*')
    title('Trial lengths')
    ylabel('Time (s)')
    xlabel('Trial number')
    legend({'Actual time', 'Trial length'})
    
    subplot(2+nargin,1,3)
    plot(del, ':*')
    title('Shuttling delay')
    ylabel('Time (s)')
    xlabel('Trial number')
    
    if nargin==2
        subplot(2+nargin,1,4)
        hold on
        objects_in_session = setdiff(unique(scoring.trialObjects), [0]);
        for o=1:length(objects_in_session)
            trials = find(scoring.trialObjects==objects_in_session(o));
            plot(1:length(trials), trial_length(trials))
        end
     end