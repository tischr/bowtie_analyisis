baseline = 0.2; % 200ms before exploration start
exploration_window = 0.2; % take first 200ms of exploration

figure
for expl_p=1:length(exploration_periods)
    start_index(expl_p) = get_closest_timestamp(exploration_periods(1,expl_p)-baseline, power_timestamps);
    exp_index
    end_index(expl_p) = get_closest_timestamp(exploration_periods(1,expl_p) + exploration_window, power_timestamps);
   
    subplot(1,4,1)
    hold on
    plot(mean_power_low_freq(start_index(expl_p):end_index(expl_p), :))
    
end

