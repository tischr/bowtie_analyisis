%% Calculate familiarity of stimuli

% clean up the mess
clear all
close all
clc

% make sure to only analyze data if there are no changed files in the
% analysis directory
[~, git_status] = system(['git -C ' get_analysis_folder ' status']);

if (~isempty(strfind(git_status, 'Changes not staged for commit:')) || ...
        ~isempty(strfind(git_status, 'Changes to be committed:')))
    error(['You changed something in the bowtie_analysis repository. '...
        'Please commit all changes before starting an analysis!'])
end
clear git_status

% get the current hash of the bowtie_analysis repository
[~, git_repository_hash_behavior] = system(['git -C ' get_analysis_folder ' rev-parse HEAD']);
fprintf('Behavior across sessions - version %s', git_repository_hash_behavior)


run = 1;
% sessions = input('Session number: ');

processor_id = 100;
experiment_folder = sprintf('N:\\Bowtie\\Run %d\\', run);
init_dir = pwd; %folder from which the script was run
analysis_folder = get_analysis_folder;

tic
disp('Analyzing behavior')
behavior = struct();
for session=1:12
    %
        disp('***************************************************************')
        fprintf('Working on dataset in %s\n\tSession %02d\n', experiment_folder, session);
        disp('***************************************************************')
    
    working_dir = sprintf('%sSession %02d', experiment_folder, session); % data directory for current session
    
    cd(working_dir)
    
    load('Session_preprocessed.mat', 'events_info', 'exploration_periods', 'stimulus_at_sample','timestamps', 'scoring', 'timestamps_t0')
    behavior(session).timestamps_t0 = timestamps_t0;
    behavior(session).timestamps = timestamps;
    behavior(session).exploration_periods = exploration_periods;
    behavior(session).stimulus_at_sample = stimulus_at_sample;
    behavior(session).experiment_start_absolute = datetime(events_info.header.date_created, 'Inputformat', 'dd-MMMM-yyyy HHmmss');
    behavior(session).exploration_periods_absolute = datetime(events_info.header.date_created, 'Inputformat', 'dd-MMMM-yyyy HHmmss') + seconds(exploration_periods);
    behavior(session).exploration_period_stimulus = zeros(size(exploration_periods, 2), 1);
    behavior(session).exploration_period_trial = zeros(size(exploration_periods, 2), 1);
    for exp_p=1:size(exploration_periods, 2)
        [behavior(session).exploration_period_stimulus(exp_p), behavior(session).exploration_period_trial(exp_p)] = get_stimulus_at_t(exploration_periods(1,exp_p), scoring);
    end
    
end
toc
% find all stimulus numbers
unique_stimuli = unique(vertcat(behavior.stimulus_at_sample));
unique_stimuli = unique_stimuli(unique_stimuli>0);

current_familiarity = zeros(size(unique_stimuli));
% last_seen = seconds(zeros(size(unique_stimuli)));
disp('Calculate familiarities')
tic
for session=1:12
    fprintf('Session %d\n', session)
    new_trial = [1; diff(behavior(session).exploration_period_trial)];
    behavior(session).familiarity = zeros(size(behavior(session).exploration_periods, 2), 1);
    %     behavior(session).absolute_familiarity = seconds(zeros(size(behavior(session).exploration_periods, 2), 1));
    
    for tr=1:length(new_trial)
        if ~behavior(session).exploration_period_stimulus(tr)
            % skip trials with no object
            continue;
        end
        if new_trial(tr)
            current_familiarity(behavior(session).exploration_period_stimulus(tr)) = current_familiarity(behavior(session).exploration_period_stimulus(tr)) + 1;
        end
        %         last_seen(behavior(session).exploration_period_stimulus(tr)) = behavior(session).exploration_periods_absolute(tr);
        %         behavior(session).absolute_familiarity(tr)
        behavior(session).familiarity(tr) = current_familiarity(behavior(session).exploration_period_stimulus(tr));
    end
    
    behavior(session).familiarity_at_sample = nan(size(timestamps));
    for t=1:length(behavior(session).timestamps)
        tmp_fam = 0;
        for exp_p=1:size(behavior(session).exploration_periods, 2)
            if         behavior(session).timestamps(t) >= behavior(session).exploration_periods(1, exp_p) && ...
                    behavior(session).timestamps(t) <= behavior(session).exploration_periods(2, exp_p)
                tmp_fam = behavior(session).familiarity(exp_p);
            end
        end
        behavior(session).familiarity_at_sample(t) = tmp_fam;
    end
    
end
toc

disp('saving...')
tic
for session=1:12
    working_dir = sprintf('%sSession %02d', experiment_folder, session); % data directory for current session
    cd(working_dir)
    save('Session_preprocessed.mat', 'behavior', 'git_repository_hash_behavior', '-append')
end
toc