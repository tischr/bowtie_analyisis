%% Remove noise from preprocessed data

% clean up the mess
clear all
close all
clc

% make sure to only analyze data if there are no changed files in the
% analysis directory
[~, git_status] = system(['git -C ' get_analysis_folder ' status']);

if (~isempty(strfind(git_status, 'Changes not staged for commit:')) || ...
        ~isempty(strfind(git_status, 'Changes to be committed:')))
    error(['You changed something in the bowtie_analysis repository. '...
        'Please commit all changes before starting an analysis!'])
end
clear git_status

% get the current hash of the bowtie_analysis repository
[~, git_repository_hash_noise_detection] = system(['git -C ' get_analysis_folder ' rev-parse HEAD']);
fprintf('Noise removal - version %s', git_repository_hash_noise_detection)


run = 1;
% sessions = input('Session number: ');

processor_id = 100;
experiment_folder = sprintf('N:\\Bowtie\\Run %d\\', run);
init_dir = pwd; %folder from which the script was run
    analysis_folder = get_analysis_folder;

    disp('noise removal')
for session=1:12
    
    disp('***************************************************************')
    fprintf('Working on dataset in %s\n\tSession %02d\n', experiment_folder, session);
    disp('***************************************************************')
    
    working_dir = sprintf('%sSession %02d', experiment_folder, session); % data directory for current session
    
    cd(working_dir)

    disp('Load preprocessed data')
    tic
    load('Session_preprocessed.mat', 'data')
    toc
    disp('Noise detection')
    
    % noise is where 30% of the abs(data) channels are > 4.5 std
    noise_at_timestamp = detect_noise_semiauto(data(:,1:64), 4.5, 0.3, 100, true);
    %% save preprocessed data
    save('Session_preprocessed.mat', 'noise_at_timestamp', 'git_repository_hash_noise_detection', '-v7.3', '-append')
end