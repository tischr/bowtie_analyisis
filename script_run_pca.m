%%  Filtered data
unique_stim = unique(stimulus_at_sample);
tmp_stim = stimulus_at_sample;
for s=1:length(unique_stim)
    if unique_stim(s) ~= 0
        tmp_stim(stimulus_at_sample == unique_stim(s)) = s;
    end
end

tmp_data = power_theta;
tmp_stim = behavior(1).familiarity_at_sample;

figuretitle = 'theta power - exploration - familiarity';

sample_selection = is_exploration & ~noise_at_timestamp & tmp_stim > 0;

figure('Name', figuretitle)
subplot(1,3,1)
pca_analysis(tmp_data(sample_selection,1:32), tmp_stim(sample_selection)); title('PrH')
subplot(1,3,2)
pca_analysis(tmp_data(sample_selection,33:64), tmp_stim(sample_selection)); title('PFC')
subplot(1,3,3)
pca_analysis(tmp_data(sample_selection,1:64), tmp_stim(sample_selection)); title('PrH+PFC')
set(gcf, 'Position',  [1, 300, 1900, 550])


% [data_pca_coeffs_prh, data_pca_score_prh, data_pca_explained_prh] = pca_analysis(tmp_data(sample_selection,1:32));
% [data_pca_coeffs_pfc, data_pca_score_pfc] = pca_analysis(tmp_data(sample_selection,33:64));
% [data_pca_coeffs_both, data_pca_score_both,  data_pca_explained_both] = pca_analysis(tmp_data(sample_selection,1:64));

% drawnow
% pause(1)
% saveas(gcf, [analysis_folder 'Analyses\\2016-06-15-pca-' figuretitle '.png'])
% clean up
clear unique_stim tmp_stim tmp_data figuretitle sample_selection

