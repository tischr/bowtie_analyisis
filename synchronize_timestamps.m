function [arduino_timestamps, scoring] = synchronize_timestamps(arduino_timestamps, scoring, trigger_timestamps)
% 
% Arduino
% ans = 
% 
%     'session_start'
%     'door_opening'
%     'door_open'
%     'animal_crossed'
%     'door_closed'
%     'trial_start'
%     'session_end'
%     
% arduino_timestamps.session_start.arduino_time
% times in ms

% oe
% ans = 
% 
%     'start_end'
%     'door_open'
%     'door_close'
%     'sound'
% times in s

% Align on end of experiment. This is because sometimes, the code was
% restarted (two experiment start triggers) or recording was started too
% late (no experiment start trigger). End of experiment should exist in
% every session in which a recrding was done
time_offset = trigger_timestamps.start_end(end)-arduino_timestamps.session_end.arduino_time/1000;

fn = fieldnames(arduino_timestamps);
for f=1:length(fn)
    for i=1:length(arduino_timestamps.(fn{f}))
        arduino_timestamps.(fn{f})(i).arduino_time_in_oe_timeframe = ...
            arduino_timestamps.(fn{f})(i).arduino_time/1000 + time_offset;
    end
end

video_time_offset = arduino_timestamps.session_end.bonsai_time/1000 ...
            - arduino_timestamps.session_end.arduino_time_in_oe_timeframe;

scoring.video_timestamps_in_oe_timeframe = scoring.video_timestamps/1000 ...
    - video_time_offset;

% fix for the first scoring versions: find the index of each trial
% timestamp again to be able to access the proper timestamp of that frame
% scoring.trial_timestamps = [trigger_timestamps.start_end(end-1) trigger_timestamps.door_close];

scoring.trial_frames = [];
scoring.trial_timestamps = []; 
for i=1:length(scoring.trialTimestamps)
    try
        scoring.trial_frames(i) = ...
         find(abs(scoring.video_timestamps/1000-scoring.trialTimestamps{i})== ...
             min(abs(scoring.video_timestamps/1000-scoring.trialTimestamps{i})));
         scoring.trial_timestamps(i) = scoring.video_timestamps_in_oe_timeframe(scoring.trial_frames(i));
    catch
        scoring.trial_frames(i) = NaN;
        scoring.trial_timestamps(i) = NaN;
    end
end

