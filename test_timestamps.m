%% test_load_arduino_timestamps
filename = 'tests\\test_load_arduino_timestamps.csv';
[result.timestamps, result.lines] = load_ardunio_timestamps(filename);
rightresult = load('tests\\test_load_arduino_timestamps.mat');
assert(isequal(result, rightresult))

%% test_calculate_trigger_timestamps
patterns = struct(...
    'start_end', [50, 25, 25, 50, 50], ...
    'door_open', [35, 35, 70], ...
    'door_close', [70, 35, 35], ...
    'sound', [35, 35, 35, 35, 35]);

test_timestamps = [50,75,100,150,200,123413,123448,123483,123553,123623,123658,123693,123728,123763,123798,123833,123868,123918,123943,123968,124018,124068];
