function topo_plot(mapping, values, hide_accelerometer)
    if nargin >= 2
        [~, ind] = sort(mapping.channel);
        scatter(mapping.visualization_x, mapping.visualization_y, mapping.diameter*8, values(ind), 'filled');
        if hide_accelerometer
           ylim([-50 max(mapping.visualization_y)+50]) ;
        end
    else
        scatter(mapping.visualization_x, mapping.visualization_y, mapping.diameter*8, 'filled');
        ylim([min(mapping.visualization_y) - 200 max(mapping.visualization_y)+200])
    end
    xlim([min(mapping.visualization_x)-100 max(mapping.visualization_x)+100])
    set(gca, 'XTickLabel', [])
    %ylabel('Distance from probe tip')
    set(gcf, 'Position', [550    50   750   950])
    %text(mapping.visualization_x+20, mapping.visualization_y, mapping.hist_B04);