function v_line = vertical_line(x, min_y, max_y, colorspec)
%%Plot a vertical line from min_y to max_y.
% If min_y or may_y are empty, they are automatically taken from the
% current axis

    if nargin < 1
        error('You need to provide at leat an x position!')
    end
    
    auto_y = get(gca, 'YLim');
    if nargin < 3 || isempty(max_y)
        max_y = auto_y(2);
    end
    if nargin < 2 || isempty(min_y)
        min_y = auto_y(1);
    end
    
    if nargin < 4
        colorspec = 'r:';
    end
    v_line = plot([x x], [min_y; max_y], colorspec);