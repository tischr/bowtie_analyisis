function frame = video_at_oe_time(video_reader, timestamp, scoring, timestamps)
    t = timestamp-timestamps(1)-scoring.video_timestamps_in_oe_timeframe(1);
    
    if t > 0 && t < video_reader.Duration
        set(video_reader, 'CurrentTime', t);
        if (hasFrame(video_reader))
            frame = readFrame(video_reader);
        else
            frame = nan(video_reader.Height, video_reader.Width);
        end
    else
        frame = nan(video_reader.Height, video_reader.Width);
    end
    
    % don't plot if frame should be returned
    if nargout == 0
        figure(9999)
        hold off
        image(frame)
        title(sprintf('Video for t=%3.2fsec', timestamp))
    end
        

        
           