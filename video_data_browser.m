function browser_figure = video_data_browser(video_reader, data, scoring, timestamps, exploration_periods, arduino_timestamps)
    
    function set_display_time(varargin)
        disp(varargin)
        [timestamp, ~] = ginput(1);
        update_display(timestamp);
    end
    
    % allows to set the display time automatically by changing the figure's
    % user data
    function set_display_time_auto(varargin)
        timestamp = get(browser_figure, 'UserData');
        update_display(timestamp);
    end
    
    function update_display(timestamp)
        frame = video_at_oe_time(video_reader, timestamp, scoring, timestamps);
        subplot(2,1,1)
        videoAxes = image(frame);
        set(get(videoAxes, 'Parent'), 'XTickLabel' , [])
        set(get(videoAxes, 'Parent'), 'YTickLabel' , [])
        title(sprintf('Video for t=%3.2fsec', timestamp))
        set(timeIndicator, 'XData', [timestamp timestamp]);
%         axes(dataZoom);
%         [~, current_trial_number, img] = get_stimulus_at_t(timestamp, scoring);
%         tmp = get(dataZoom, 'Children');
%         set(tmp(1), 'CData', img);
    end
    
    browser_figure = figure('Name', 'Video and data browser');
    addlistener(browser_figure, 'UserData', 'PostSet', @set_display_time_auto);
    
    set(browser_figure, 'Color', 'white')
    
    frame = nan(video_reader.Height, video_reader.Width);
    
    % video
    subplot(2,1,1)
    title('Video')
    videoAxes = image(frame);
    set(get(videoAxes, 'Parent'), 'XTickLabel' , [])
    set(get(videoAxes, 'Parent'), 'YTickLabel' , [])
    
    % data
    subplot(2,1,2)
    title('Channel 1')
    plotAxes = plot(timestamps, data(:,1));
    axis tight
    prettify_plot(gca);
    
    tmp = get(gca, 'YLim');
    y_min = tmp(1); y_max = tmp(2); clear tmp
    
    hold on
    timeIndicator = vertical_line(0, y_min, y_max, 'k');
%     
%     [~, current_trial_number, img] = get_stimulus_at_t(0, scoring);
%     dataZoom = axis();
%     image(img)
%     dataZoom = gca;
%     title(sprintf('Trial: %d', current_trial_number))
    
    set(plotAxes, 'ButtonDownFcn', @set_display_time);
    
    % plot trial starting times - currently they are 2-indexed (stupid
    % shift due to the messing around between 0 and 1 indexing in
    % openephys/python/matlab
    trialLines = [];
    for i=2:length(scoring.trial_timestamps)
        trialLines(end+1) = vertical_line(scoring.trial_timestamps(i), y_min, y_max, 'b:');
    end
    
    % display trial limits and exploration times
    
    % automatic control
    play_button = uicontrol('String', '> Play >', 'Position', [150 520 100 20]);
    stop_button = uicontrol('String', '| Stop |', 'Position', [260 520 100 20]);
    is_playing = false;
    
    set(play_button, 'Callback', @play_movie);
    set(stop_button, 'Callback', @stop_movie);
    
    function play_movie(varargin)
        is_playing = true;
        dt = 1/video_reader.FrameRate;
        while is_playing
%             frame_time = tic;
            new_time = video_reader.CurrentTime + dt;
            update_display(new_time);
            drawnow;
%             pause(dt-toc(frame_time));
        end
    end
    
    function stop_movie(varargin)
        is_playing = false;
    end
    
    
end
    